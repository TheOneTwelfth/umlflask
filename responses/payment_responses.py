from responses.generic_responses import BaseResponse


def serialize_payment(obj):
    obj_dict = vars(obj)
    return {i: obj_dict[i] for i in obj_dict if i != '_sa_instance_state'}


class PaymentResponse(BaseResponse):
    payload_name = 'payment'

    def __init__(self, ok, payment):
        self.ok = ok

        if payment:
            self.payload = serialize_payment(payment)
        else:
            self.payload = None


class PaymentListResponse(BaseResponse):
    payload_name = 'payments'

    def __init__(self, ok, payments):
        self.ok = ok

        if payments:
            self.payload = [serialize_payment(result) for result in payments]
        else:
            self.payload = None
