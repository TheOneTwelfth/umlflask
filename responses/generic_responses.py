from flask import jsonify


class BaseResponse:
    ok = False

    payload_name = 'payload'
    payload = object()

    def send(self):
        response_dict = {
            'ok': self.ok,
            self.payload_name: self.payload
        }

        return jsonify(response_dict)


class DictResponse(BaseResponse):
    payload_name = 'data'

    def __init__(self, ok, data):
        self.ok = ok
        self.payload = data
