from responses.generic_responses import BaseResponse


def serialize_card(obj):
    obj_dict = vars(obj)
    return {i: obj_dict[i] for i in obj_dict if i != '_sa_instance_state'}


class CardResponse(BaseResponse):
    payload_name = 'card'

    def __init__(self, ok, card):
        self.ok = ok

        if card:
            self.payload = serialize_card(card)
        else:
            self.payload = None


class CardListResponse(BaseResponse):
    payload_name = 'cards'

    def __init__(self, ok, cards):
        self.ok = ok

        if cards:
            self.payload = [serialize_card(result) for result in cards]
        else:
            self.payload = None
