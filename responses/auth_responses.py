from responses.generic_responses import BaseResponse


class AuthResponse(BaseResponse):
    payload_name = 'token'

    def __init__(self, ok, token):
        self.ok = ok
        self.payload = token
