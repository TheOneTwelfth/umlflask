from flask import abort, request

from app import db
from models import Card
from views.generic_views import BaseJWTView
from responses.card_responses import CardResponse, CardListResponse


class CardView(BaseJWTView):
    required_fields = ['card_number', 'expiry_month', 'expiry_year', 'cvv']

    def get(self):
        user = self.get_user()
        cards = user.cards

        return CardListResponse(
            ok=True,
            cards=cards
        ).send()

    def post(self):
        if not self.check_required_fields():
            return abort(400)

        data = request.json

        holder = self.get_user()

        new_card = Card(
            holder=holder.login,
            card_number=data['card_number'],
            expiry_month=data['expiry_month'],
            expiry_year=data['expiry_year'],
            cvv=data['cvv']
        )

        card_response = CardResponse(
            ok=True,
            card=new_card
        )

        db.session.add(new_card)
        db.session.commit()

        return card_response.send()

    def delete(self, object_id):
        card = Card.get_card_by_id(object_id)

        if not card:
            return CardResponse(
                ok=False,
                card=None
            ).send()

        card_response = CardResponse(
            ok=True,
            card=card
        )

        db.session.delete(card)
        db.session.commit()

        return card_response.send()
