from flask import abort, request
from flask.views import MethodView
from flask_jwt_extended import get_jwt_identity, jwt_required

from models import User


class BaseMethodView(MethodView):
    methods = ['GET', 'POST', 'PUT', 'DELETE']
    required_fields = []

    def get(self):
        return abort(400)

    def post(self):
        return abort(400)

    def put(self, object_id):
        return abort(400)

    def delete(self, object_id):
        return abort(400)

    def check_required_fields(self):
        data = request.json
        if not data:
            return False

        return all(field in data for field in self.required_fields)


class BaseJWTView(BaseMethodView):
    decorators = [jwt_required]

    @staticmethod
    def get_user():
        login = get_jwt_identity()
        return User.get_user_by_login(login)
