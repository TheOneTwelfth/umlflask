from flask import abort, request

from app import db
from models import User, Payment
from views.generic_views import BaseJWTView
from responses.payment_responses import PaymentListResponse, PaymentResponse


class PaymentView(BaseJWTView):
    required_fields = ['recipient', 'amount']

    def get(self):
        user = self.get_user()
        payments = user.payments

        return PaymentListResponse(
            ok=True,
            payments=payments
        ).send()

    def post(self):
        if not self.check_required_fields():
            return abort(400)

        data = request.json

        sender = self.get_user()
        recipient = User.get_user_by_login(data['recipient'])

        if not recipient or data['amount'] <= 0:
            return PaymentResponse(
                ok=False,
                payment=None
            ).send()

        try:
            message = data['message']
        except KeyError:
            message = None

        new_payment = Payment(
            sender=sender.login,
            recipient=recipient.login,
            amount=data['amount'],
            message=message
        )

        payment_response = PaymentResponse(
            ok=True,
            payment=new_payment
        )

        db.session.add(new_payment)
        db.session.commit()

        return payment_response.send()

    def put(self, object_id):
        payment = Payment.get_payment_by_id(object_id)

        if not payment:
            return PaymentResponse(
                ok=False,
                payment=None
            ).send()

        payment.status = True

        payment_response = PaymentResponse(
            ok=True,
            payment=payment
        )

        db.session.commit()

        return payment_response.send()
