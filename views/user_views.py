import datetime

from flask import request, abort
from flask_jwt_extended import create_access_token

from app import db
from views.generic_views import BaseMethodView, BaseJWTView
from models import User
from responses.auth_responses import AuthResponse
from responses.generic_responses import DictResponse


class RegisterView(BaseMethodView):
    required_fields = ['login', 'password', 'name', 'surname']

    def post(self):
        data = request.json

        if not self.check_required_fields():
            return abort(400)

        existing_user = User.get_user_by_login(data['login'])

        if existing_user is not None:
            return AuthResponse(
                ok=False,
                token=None
            ).send()

        new_user = User(
            login=data['login'],
            password=data['password'],
            name=data['name'],
            surname=data['surname']
        )

        db.session.add(new_user)
        db.session.commit()

        return AuthResponse(
            ok=True,
            token=create_access_token(identity=new_user.login, expires_delta=datetime.timedelta(days=365))
        ).send()


class AuthView(BaseMethodView):
    required_fields = ['login', 'password']

    def post(self):
        data = request.json

        if not self.check_required_fields():
            return abort(400)

        user = User.get_user_by_login(data['login'])

        if not user:
            return AuthResponse(
                ok=False,
                token=None
            ).send()

        if data['password'] != user.password:
            return abort(401)

        return AuthResponse(
            ok=True,
            token=create_access_token(identity=user.login, expires_delta=datetime.timedelta(days=365))
        ).send()


class GetUserInfoView(BaseJWTView):
    def get(self):
        user = self.get_user()

        return DictResponse(
            ok=True,
            data={
                'name': user.name,
                'surname': user.surname
            }
        ).send()
