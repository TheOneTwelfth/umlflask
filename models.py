from app import db


class User(db.Model):
    __tablename__ = 'user'

    login = db.Column(db.Text, primary_key=True)
    password = db.Column(db.Text)

    name = db.Column(db.Text)
    surname = db.Column(db.Text)

    payments = db.relationship('Payment', backref='user', lazy=True)
    cards = db.relationship('Card', backref='user', lazy=True)

    def __init__(self, login, password, name, surname):
        self.login = login
        self.password = password
        self.name = name
        self.surname = surname

    @staticmethod
    def get_user_by_login(login):
        return __class__.query.filter_by(login=login).first()


class Payment(db.Model):
    __tablename__ = 'payment'

    id = db.Column(db.Integer, primary_key=True)

    sender = db.Column(db.Text, db.ForeignKey('user.login'))
    recipient = db.Column(db.Text)

    amount = db.Column(db.Float)
    message = db.Column(db.Text, nullable=True)

    status = db.Column(db.Boolean)

    def __init__(self, sender, recipient, amount, message):
        self.sender = sender
        self.recipient = recipient
        self.amount = amount
        self.message = message

        self.status = False

    @staticmethod
    def get_payment_by_id(payment_id):
        return __class__.query.filter_by(id=payment_id).first()


class Card(db.Model):
    __tablename__ = 'card'

    id = db.Column(db.Integer, primary_key=True)
    holder = db.Column(db.Text, db.ForeignKey('user.login'))

    card_number = db.Column(db.Text)
    expiry_month = db.Column(db.Text)
    expiry_year = db.Column(db.Text)
    cvv = db.Column(db.Text)

    @staticmethod
    def get_card_by_id(card_id):
        return __class__.query.filter_by(id=card_id).first()
