import os


basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SQLALCHEMY_DATABASE_URI = 'postgresql://dev:dev@10.62.32.3:5432/uml_flask'
    JWT_SECRET_KEY = 'methods-inside-main'
