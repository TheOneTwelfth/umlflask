from flask import Flask
from flask_jwt_extended import JWTManager
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('config.Config')
db = SQLAlchemy(app)
jwt = JWTManager(app)


def setup():
    from views.user_views import AuthView, RegisterView, GetUserInfoView
    from views.payment_views import PaymentView
    from views.cards_view import CardView

    db.create_all()

    app.add_url_rule('/auth/login', view_func=AuthView.as_view('login'))
    app.add_url_rule('/auth/register', view_func=RegisterView.as_view('register'))
    app.add_url_rule('/user', view_func=GetUserInfoView.as_view('user'))

    payment_view = PaymentView.as_view('payment')
    app.add_url_rule('/payment/', view_func=payment_view, methods=['GET', 'POST'])
    app.add_url_rule('/payment/<int:object_id>', view_func=payment_view, methods=['PUT'])

    cards_view = CardView.as_view('cards')
    app.add_url_rule('/cards/', view_func=cards_view, methods=['GET', 'POST'])
    app.add_url_rule('/cards/<int:object_id>', view_func=cards_view, methods=['DELETE'])


setup()


if __name__ == '__main__':
    app.run()
